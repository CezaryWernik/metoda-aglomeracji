function [ Oi ] = getObjIndex( S, S_INDEX )
   S_TYPE_O=1;
   S_TYPE_S=2;
   S_OB1_TYPE=2;
   S_OB1_INDEX=3;
   S_OB2_TYPE=4;
   S_OB2_INDEX=5;
   
   Oi=[];
   if(S(S_INDEX,S_OB1_TYPE)==S_TYPE_O && S(S_INDEX,S_OB2_TYPE)==S_TYPE_O)
      Oi=[Oi S(S_INDEX,S_OB1_INDEX) S(S_INDEX,S_OB2_INDEX)];
   end
   
   if(S(S_INDEX,S_OB1_TYPE)==S_TYPE_O && S(S_INDEX,S_OB2_TYPE)==S_TYPE_S)
      Oi=[Oi S(S_INDEX,S_OB1_INDEX) getObjIndex( S, S(S_INDEX,S_OB2_INDEX) )];
   end
   
   if(S(S_INDEX,S_OB1_TYPE)==S_TYPE_S && S(S_INDEX,S_OB2_TYPE)==S_TYPE_O)
      Oi=[Oi getObjIndex( S, S(S_INDEX,S_OB1_INDEX) ) S(S_INDEX,S_OB2_INDEX)];
   end
   
   if(S(S_INDEX,S_OB1_TYPE)==S_TYPE_S && S(S_INDEX,S_OB2_TYPE)==S_TYPE_S)
      Oi=[Oi getObjIndex( S, S(S_INDEX,S_OB1_INDEX) ) getObjIndex( S, S(S_INDEX,S_OB2_INDEX) )];
   end
end

