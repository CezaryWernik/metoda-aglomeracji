close all 
clear all
clc

%DANE OBIEKT�W
%{
O = [39.8 38.0 22.2 23.2;
     53.7 37.2 18.7 18.5;
     47.3 39.8 23.3 22.1;
     41.7 37.6 22.8 22.3;
     44.7 38.5 24.8 24.4;
     47.9 39.8 22.0 23.3];
  %}


O = [39.8 38.0 19.2 23.2;
     47.6 39.8 22.4 22.1;
     41.7 37.6 21.0 22.3;
     40.7 38.5 24.8 23.4;
     47.9 39.8 22.0 23.3;
     39.7 38.0 20.0 22.3;
     48.0 39.9 23.3 22.1;
     39.5 37.9 20.2 23.3;
     47.7 39.7 22.7 23.0;
     47.8 39.8 22.0 23.3;
     47.9 39.9 22.4 22.7;
     39.4 37.6 19.8 22.5;
     39.6 38.1 18.8 23.2;
     48.1 39.7 23.0 22.3];
  
%Transponowanie macierzy z danymi obiekt�w
disp('Obiekty przed standaryzacj�');
O=O'

%N obiekt�w
%p atrybut�w
[p,N]=size(O);

%Standaryzacja atrybut�w obiekt�w
for n=1:p
   m=mean(O(n,:));
   s=std(O(n,:));
   O(n,:)=(O(n,:)-m)./s;
end
disp('Obiekty po standaryzacji');
O

%Definicja akumulatora - akumulator b�dzie zawiera� brane pod uwag� w danej
%iteracji skupienia i obiekty
A=zeros(2,N);
A_TYPE   = 1; %Sta�a zawieraj�ca nr wiersza akumulatora w kt�rym s� typy
A_TYPE_O = 1; %Sta�a okre�lajaca pozycj� jako obiekt
A_TYPE_S = 2; %Sta�a okre�lajaca pozycj� jako skupienie
A_INDEX  = 2; %Sta�a zawieraj�ca nr wiersza akumulatora w kt�rym s� indeksy
A_EMPTY = 0; %Puste pole akumulatora
% Pocz�tkowa inicjacja akumulatora
A(1,:)=A_TYPE_O;
A(2,:)=1:N;

%Definicja tablicy skupie�
S = zeros(N,7);
S_INDEX = 1; %Kolumna z indeksem
S_OB1_TYPE = 2; %Kolumna z typem pozycji pierwszej
S_OB1_INDEX = 3; %Kolumna z indeksem pozycji pierwszej
S_OB2_TYPE = 4; %Kolumna z typem pozycji drugiej
S_OB2_INDEX = 5; %Kolumna z indeksem pozycji drugiej
S_STATUS = 6; %Kolumna okreslaj�ca status skupienia
S_D = 7; %Kolumna z miar� odleg�o�ci skupienia
S_USE = 1; %Status: w u�yciu (in use)
S_IOC = 2; %Status: skupienie w innej selekcji (in other concentration)
S_EMPTY = 0; %Status: skupienie nie u�ywane/puste 
%Pocz�tkowa linicjalizacja tablicy skupie�
S(:,S_INDEX) = 1:N; 
   
for I=1:N-1
   D=zeros(N-I+1);
   for x=1:size(A,2)
      for y=1:size(A,2)
         if(A(A_TYPE,x)==A_TYPE_O && A(A_TYPE,y)==A_TYPE_O)
            %gdy tylko obiekty
            D(x,y)=distance( O, A(A_INDEX,x), A(A_INDEX,y), 'euklides' );
         else
            if(A(A_TYPE,x)==A_TYPE_S && A(A_TYPE,y)==A_TYPE_S)
               %gdy oba skupienia
               Ox=getObjIndex( S, A(A_INDEX,x));
               Oy=getObjIndex( S, A(A_INDEX,y));
               D(x,y)=distance( O, Ox, Oy, 'euklides' );
            end
            if(A(A_TYPE,x)==A_TYPE_O && A(A_TYPE,y)==A_TYPE_S)
               %gdy tylko skupienie po y
               Ox=A(A_INDEX,x);
               Oy=getObjIndex( S, A(A_INDEX,y));
               D(x,y)=distance( O, Ox, Oy, 'euklides' );
            end
            if(A(A_TYPE,x)==A_TYPE_S && A(A_TYPE,y)==A_TYPE_O)
               %gdy tylko skupienie po x
               Ox=getObjIndex( S, A(A_INDEX,x));
               Oy=A(A_INDEX,y);
               D(x,y)=distance( O, Ox, Oy, 'euklides' );
            end
         end
      end
   end
   disp('________________________________________________________________');
   disp(strcat('I=',num2str(I)));
   disp('ODLEG�O�CI = ');
   disp(D);
   
   %Okre�lenie najmniejszej odleg�o�ci mi�dzy obiektami/skupieniami
   D=tril(D)+triu(ones(size(D)))*max(max(D))*2;
   [x_min,y_min]=find(D==min(min(D)));
   disp('[ x_min, y_min ] = ');
   disp([x_min,y_min]);

   %Sktualizacja skupie� 
   S(I,:)=[I A(A_TYPE,x_min) A(A_INDEX,x_min) A(A_TYPE,y_min) A(A_INDEX,y_min) S_USE D(x_min,y_min)];

   if( A( A_TYPE, x_min ) == A_TYPE_S )
      S(A(A_INDEX,x_min),S_STATUS)=S_IOC;
   end
   if(A(A_TYPE,y_min)==A_TYPE_S)
      S(A(A_INDEX,y_min),S_STATUS)=S_IOC;
   end
   disp('SKUPIENIA = ');
   disp(S(S(:,S_STATUS)~=S_EMPTY,:));

   %Redukcja pozycji w akumulatorze
   A(:,x_min)=[];
   A(:,y_min)=[];

   %Aktualizacja akumulatora
   A_OLD=A;
   A=zeros(2,1);
   Ai=1;
   %W tej p�tli do nowego akumulatora dodaj� skupienia
   for Si=1:I+1 
      if(S(Si,S_STATUS)==S_USE)
         A(:,Ai)=[A_TYPE_S,S(Si,S_INDEX)];
         Ai=Ai+1;
      elseif(S(Si,S_STATUS)==S_EMPTY)
         break;
      end
   end
   %W tej p�tli do nowego akumulatora dodaj� nieskupione obiekty
   for Ai_OLD=1:size(A_OLD,2) 
      if(A_OLD(A_TYPE,Ai_OLD)~=A_TYPE_S && A_OLD(A_TYPE,Ai_OLD)~=A_EMPTY)
         A(:,Ai)=[A_TYPE_O A_OLD(A_INDEX,Ai_OLD)];
         Ai=Ai+1;
      end
   end
   disp('AKUMULATOR = ');
   disp(A)
end

%Rysowanie
Oi = getObjIndex( S, N-1 );
LEAF=[Oi;1:N];
figure;
plot(1:N,zeros(N,1),'.b')
hold on
for I=1:N
   t=text(LEAF(2,I),0,strcat('O_{',num2str(LEAF(1,I)),'}'));
   t.FontSize = 9;
   t.Color = 'b'
end
axis equal
Sxy=[];
for I=1:N-1
   
   if(S(I,S_OB1_TYPE)==A_TYPE_O && S(I,S_OB2_TYPE)==A_TYPE_O)
      Sxy(I,:)=[I (LEAF(2,(LEAF(1,:)==S(I,S_OB1_INDEX)))+LEAF(2,(LEAF(1,:)==S(I,S_OB2_INDEX))))/2 I];
      plot([LEAF(2,(LEAF(1,:)==S(I,S_OB1_INDEX))) Sxy(I,2)],[0 I],'-k')
      plot([LEAF(2,(LEAF(1,:)==S(I,S_OB2_INDEX))) Sxy(I,2)],[0 I],'-k')
   end
   if(S(I,S_OB1_TYPE)==A_TYPE_O && S(I,S_OB2_TYPE)==A_TYPE_S)
      Sxy(S(I,S_OB2_INDEX),2)
      Sxy(I,:)=[I ((LEAF(2,(LEAF(1,:)==S(I,S_OB1_INDEX)))+Sxy(S(I,S_OB2_INDEX),2))/2) I];
      plot([LEAF(2,(LEAF(1,:)==S(I,S_OB1_INDEX))) Sxy(I,2)],[0 I],'-k')
      plot([Sxy(S(I,S_OB2_INDEX),2) Sxy(I,2)],[Sxy(S(I,S_OB2_INDEX),3) I],'-k')
   end
   if(S(I,S_OB1_TYPE)==A_TYPE_S && S(I,S_OB2_TYPE)==A_TYPE_O)
      Sxy(I,:)=[I ((LEAF(2,(LEAF(1,:)==S(I,S_OB2_INDEX)))+Sxy(S(I,S_OB1_INDEX),2))/2) I];
      plot([LEAF(2,(LEAF(1,:)==S(I,S_OB2_INDEX))) Sxy(I,2)],[0 I],'-k')
      plot([Sxy(S(I,S_OB1_INDEX),2) Sxy(I,2)],[Sxy(S(I,S_OB1_INDEX),3) I],'-k')
   end
   if(S(I,S_OB1_TYPE)==A_TYPE_S && S(I,S_OB2_TYPE)==A_TYPE_S)
      Sxy(I,:)=[I ((Sxy(S(I,S_OB1_INDEX),2)+Sxy(S(I,S_OB2_INDEX),2))/2) I]
      plot([Sxy(S(I,S_OB1_INDEX),2) Sxy(I,2)],[Sxy(S(I,S_OB1_INDEX),3) I],'-k')
      plot([Sxy(S(I,S_OB2_INDEX),2) Sxy(I,2)],[Sxy(S(I,S_OB2_INDEX),3) I],'-k')
   end
   plot(Sxy(I,2),Sxy(I,3),'.r')
   t = text(Sxy(I,2),Sxy(I,3),strcat('S_{',num2str(I),'}(',num2str(S(I,S_D)),')'));
   t.FontSize = 9;
   t.Color = 'r'
end

axis([-1 N+1 -1 N+1])