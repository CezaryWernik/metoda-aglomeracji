function [ d ] = distance( O, Ox, Oy, d_type )
   switch(d_type)
      case 'power' 
         %pot�gowa
         p=2;
         r=1/3;
         fd=@(Ox,Oy)((sum((Ox-Oy).^p))^r);
      case 'euklides' 
         %euklidesowa
         fd=@(Ox,Oy)(sqrt(sum((Ox-Oy).^2)));  
      case 's_euklides'
         %kwadrat euklidesowej
         fd=@(Ox,Oy)(sum((Ox-Oy).^2));
      case 'manhatan'
         %manhatan
         fd=@(Ox,Oy)(sum(abs(Ox-Oy)));
      case 'czebyszew' 
         %czebyszewa
         fd=@(Ox,Oy)(max(abs(Ox-Oy)));
      otherwise
         %euklidesowa
         fd=@(Ox,Oy)(sqrt(sum((Ox-Oy).^2)));
   end
   nx=size(Ox,2);
   ny=size(Oy,2);
   d=zeros(nx*ny,1);
   i=1;
   for x=1:nx
      for y=1:ny
         d(i)=fd(O(:,Ox(x)),O(:,Oy(y)));
         i=i+1;
      end
   end
   d=min(d);
end

